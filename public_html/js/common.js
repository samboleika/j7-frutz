$(document).ready(function() {
    var upload_path = $('.upload-path');
    $('.upload').on('change', function() {
        if(this.files != undefined){
            upload_path.html(this.files[0].name);
        }
        else{
            upload_path.html('Прикреплено');
        }
    });
    $('.toggle-menu').on('click', function(e){
        e.preventDefault();
        var menu = $('.menu');
        if(menu.hasClass('show')){
            menu.removeClass('show');
        }
        else{
            menu.addClass('show');
        }
    });
    
    $('.phone-mask').mask('+7(000)000-00-00');
    
    $('.checkbox-block').on('click', function(){
        $(this).find('input[type="checkbox"]').click();
        if($(this).find('input[type="checkbox"]').is(':checked')){
            $(this).addClass('checkbox-on');
        }
        else{
            $(this).removeClass('checkbox-on');
        }
    });
    
    $('.input-radio-block').on('click', function(){
        var name = $(this).find('input[type="radio"]').attr('name');
        $('input[name="'+name+'"]').closest('.input-radio-block').removeClass('radio-on');
        $(this).find('input[type="radio"]').prop("checked", true);
        if($(this).find('input[type="radio"]').is(':checked')){
            $(this).addClass('radio-on');
        }
        else{
            $(this).removeClass('radio-on');
        }
    });
    
    if (typeof $.fn.select2 !== 'undefined') {
        $('select').select2({minimumResultsForSearch: -1});
    }
    
    $('.fancybox').fancybox({minWidth: 580});
    $('.j7-fancybox-close').on('click', function(){
        $.fancybox.close();
    });
    
    if (typeof $.fn.slick !== 'undefined') {
        $('.slider-for').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          fade: true,
          asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
          slidesToShow: 14,
          slidesToScroll: 1,
          asNavFor: '.slider-for',
          dots: true,
          arrows: false,
          focusOnSelect: true
        });
    }
})

function j7_modal(id){
    $.fancybox.open({href: '#' + id, wrapCSS: 'modal-'+id});
}